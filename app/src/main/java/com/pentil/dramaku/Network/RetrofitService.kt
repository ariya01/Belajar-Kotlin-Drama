package com.pentil.dramaku.Network

import com.pentil.dramaku.Object.Welcome
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitService{
    @GET("/v1/")
    fun getHome():Deferred<Response<Welcome>>
}