package com.pentil.dramaku.Activity.Home

import com.pentil.dramaku.Object.Home

interface HomeView{
    fun init()
    fun data(listdata:MutableList<Home>)
}