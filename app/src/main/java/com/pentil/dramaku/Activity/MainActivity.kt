package com.pentil.dramaku.Activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.pentil.dramaku.Activity.Home.HomeView
import com.pentil.dramaku.Object.Home
import com.pentil.dramaku.R

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
