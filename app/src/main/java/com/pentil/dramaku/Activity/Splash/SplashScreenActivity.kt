package com.pentil.dramaku.Activity.Splash

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_splash_screen.*
import android.graphics.Typeface
import android.content.Intent
import android.os.Handler
import com.pentil.dramaku.Activity.Home.HomeActivity
import com.pentil.dramaku.Network.RetrofitServer
import com.pentil.dramaku.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException


class SplashScreenActivity : AppCompatActivity(), SplashView {
    lateinit var presenter: SplashPresenter
    override fun init() {
        val custom_font = Typeface.createFromAsset(assets, "fonts/Helvetica-Bold.ttf")
        tv_splash_title.setTypeface(custom_font)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        presenter = SplashPresenter(this)
        presenter.init()
        Handler().postDelayed({
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        },1000)
        val service = RetrofitServer.makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val request = service.getHome()
            withContext(Dispatchers.Main) {
                try {
                    val response = request.await()
                    if (response.isSuccessful) {
//                        Toast.makeText(this@SplashScreenActivity, response.body()?.toString(), Toast.LENGTH_SHORT).show()
                    } else {
//                        Toast.makeText(this@SplashScreenActivity, "cek", Toast.LENGTH_SHORT).show()
                    }
                } catch (e: HttpException) {
//                    Toast.makeText(this@SplashScreenActivity, "cek", Toast.LENGTH_SHORT).show()
                } catch (e: Throwable) {
//                    Toast.makeText(this@SplashScreenActivity, "cek", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}
