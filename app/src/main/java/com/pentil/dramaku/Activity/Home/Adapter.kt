package com.pentil.dramaku.Activity.Home

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pentil.dramaku.Object.Home
import com.pentil.dramaku.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.rc_main.view.*

class Adapter(var data:MutableList<Home>,val contex:Context,
              private var itemClick:(Home)->Unit): RecyclerView.Adapter<Adapter.ViewHolder>() {
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bind(data[p1])
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.rc_main,p0,false)
        return ViewHolder(view,itemClick)
    }

    override fun getItemCount(): Int {
       return data.size
    }

    class ViewHolder(view:View,val itemClick: (Home) -> Unit):RecyclerView.ViewHolder(view) {
        fun bind(data: Home){
            with(itemView)
            {
                tv_rc_title.text = data.nama
                tv_rc_episode.text = data.last
                tv_rc_overview.text = data.overview
                tv_rc_runtime.text =data.run
                Picasso.get().load(data.poster).resize(100, 120).centerCrop().into(im_rc_poster)
                itemView.setOnClickListener { itemClick(data) }
            }
        }
    }

    fun update(data:MutableList<Home>)
    {
        this.data.clear()
        this.data = data
        notifyDataSetChanged()
    }

    fun add (data: Home)
    {
        this.data.add(data)
        notifyDataSetChanged()
    }
    fun add (data: MutableList<Home>)
    {
        this.data.addAll(data)
        notifyDataSetChanged()
    }
}
