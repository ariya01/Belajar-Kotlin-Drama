package com.pentil.dramaku.Activity.Home

import com.pentil.dramaku.Object.Home

class HomePresenter(val View: HomeView)
{
    var listdata:MutableList<Home> = mutableListOf()
    fun init()
    {
        View.init()
    }
    fun data()
    {

        val data = Home("1","Heachi","Set during the Joseon Dynasty period, four " + "people from different walks of life","Monday & Tuesday","9","http://asianwiki.com/images/b/bb/Haechi-mp01.jpg")
        val data1 = Home("2","Vagabond","The story of an ordinary man who got involved in a case of a civilian plane crash. In the middle of a hidden truth, he tries to dig up a huge national corruption.","-","-","https://i.mydramalist.com/Xe37gf.jpg")
        val data2 = Home("3","Touch Your Heart","Oh Yoon-Seo is a popular actress. She is famous for her beautiful appearance, but her acting is bad. She gets involved in a scandal with the son from a chaebol family. Her acting career declines precipitously.","Wednessay & Thusday","16","http://asianwiki.com/images/1/16/Touch_Your_Heart-p0001.jpg")
        val data3 = Home("4","Legal High","Ko Tae-Rim (Jin Goo) is a lawyer. He is arrogant and makes biting remarks, but he has a 100% winning rate","Fridays & Saturdays","20","http://asianwiki.com/images/2/24/Legal_High_%28Korean_Drama%29-mp1.jpg")
        val data4 = Home("5","Doctor Prisoner","Na Yi-Je (Namgung Min) is a surgeon with excellent skills and he also is a kind person. He gets involved in a medical accident which he did not cause.","Wednessay & Thusday","12","http://asianwiki.com/images/7/7a/Doctor_Prisoner-p0001.jpg")
        val data5 = Home("6","Memories of the Alhambra","Yoo Jin-Woo (Hyun-Bin) is the CEO of an investment company. He has good sense at his work, a strong adventurous spirit and a strong desire for winning.","Saturday & Sunday","10","http://asianwiki.com/images/0/01/Memories_of_the_Alhambra-P1.jpg")

        listdata.add(data)
        listdata.add(data1)
        listdata.add(data2)
        listdata.add(data3)
        listdata.add(data4)
        listdata.add(data5)
        View.data(listdata)
    }
}